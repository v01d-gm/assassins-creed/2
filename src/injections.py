from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm

from memory import game_state_pointer, secondary_coords_array_manager, waypoint_coords_pointer


update_game_state_pointer = AllocatingCodeInjection(
    pm.base_address + 0x95634A,
    f"""
        mov ecx, [esi + 0x18]
        mov eax, [ecx + 0x8]

        mov [{game_state_pointer}], ecx
    """,
    original_code_length=6,
)


update_waypoint_coords_pointer = AllocatingCodeInjection(
    pm.base_address + 0x8D0142,
    f"""
        mov [{waypoint_coords_pointer}], ecx

        mov eax, [ebx]
        mov edx, [eax]
        push ecx
        mov ecx, ebx
    """,
    original_code_length=7,
)

update_secondary_coords_array = AllocatingCodeInjection(
    pm.base_address + 0x24F494,
    f"""
        {secondary_coords_array_manager.generate_asm_append_code("eax")}

        mov byte [esi + 0x100], 0x1
    """,
    original_code_length=7,
)
