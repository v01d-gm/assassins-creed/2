from dearpygui import dearpygui as dpg
from trainerbase.gui import (
    GameObjectUI,
    SeparatorUI,
    SpeedHackUI,
    TeleportUI,
    TextUI,
    add_components,
    add_hotkey,
    simple_trainerbase_menu,
)
from trainerbase.tts import say

from objects import (
    god_mode,
    invisible,
    knives,
    medicine,
    money,
    pistol_bullets,
    poison_darts,
    smoke_bombs,
    waypoint_x,
    waypoint_y,
)
from teleport import tp


HOTKEY_GO_TO_WAYPOINT = "End"


@simple_trainerbase_menu("Assassin's Creed 2", 700, 300)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                TextUI("Doesn't work in combat"),
                GameObjectUI(invisible, "Invisible"),
                TextUI("128 - Off, 129 - On"),
                GameObjectUI(god_mode, "God Mode", default_setter_input_value=129),
                SeparatorUI(),
                SpeedHackUI(),
            )

        with dpg.tab(label="Inventory"):
            add_components(
                GameObjectUI(money, "Money", default_setter_input_value=1_000_000),
                GameObjectUI(poison_darts, "Poison Darts", default_setter_input_value=10),
                GameObjectUI(pistol_bullets, "Pistol Bullets", default_setter_input_value=10),
                GameObjectUI(medicine, "Medicine", default_setter_input_value=10),
                GameObjectUI(smoke_bombs, "Smoke Bombs", default_setter_input_value=10),
                GameObjectUI(knives, "Knives", default_setter_input_value=15),
            )

        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp, "Insert", "Home", "k"))

        with dpg.tab(label="Waypoint Teleport"):
            add_components(
                TextUI(f"[{HOTKEY_GO_TO_WAYPOINT}] Go To Waypoint"),
                GameObjectUI(waypoint_x, "Waypoint X"),
                GameObjectUI(waypoint_y, "Waypoint Y"),
            )

            add_hotkey(HOTKEY_GO_TO_WAYPOINT, on_hotkey_go_to_waypoint_press)


def on_hotkey_go_to_waypoint_press():
    tp.goto_waypoint()
    say("Teleported to waypoint")
