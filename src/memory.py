from trainerbase.common import ASMArrayManager
from trainerbase.memory import POINTER_SIZE, allocate_pointer


game_state_pointer = allocate_pointer()
waypoint_coords_pointer = allocate_pointer()
secondary_coords_array_manager = ASMArrayManager(100, POINTER_SIZE, [0x30])
