from math import isclose

from trainerbase.gameobject import GameFloat
from trainerbase.scriptengine import ScriptEngine, enabled_by_default

from memory import secondary_coords_array_manager
from objects import primary_player_x, primary_player_y, secondary_player_x, secondary_player_y, secondary_player_z


asm_array_script_engine = ScriptEngine(delay=0.5)


@enabled_by_default
@asm_array_script_engine.simple_script
def update_pointers():
    for possible_x_address in secondary_coords_array_manager:
        is_x_equal = isclose(GameFloat(possible_x_address, is_tracked=False).value, primary_player_x.value)
        is_y_equal = isclose(GameFloat(possible_x_address + 0x4, is_tracked=False).value, primary_player_y.value)

        if is_x_equal and is_y_equal:
            secondary_player_x.address = possible_x_address
            secondary_player_y.address = possible_x_address + 0x4
            secondary_player_z.address = possible_x_address + 0x8
