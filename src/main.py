from trainerbase.main import run

from gui import run_menu
from injections import update_game_state_pointer, update_secondary_coords_array, update_waypoint_coords_pointer
from scripts import asm_array_script_engine


def on_initialized():
    update_game_state_pointer.inject()
    update_waypoint_coords_pointer.inject()
    update_secondary_coords_array.inject()

    asm_array_script_engine.start()


if __name__ == "__main__":
    run(run_menu, on_initialized)
