from time import sleep

from trainerbase.common import Number, Teleport

from objects import (
    force_god_mode,
    primary_player_x,
    primary_player_y,
    primary_player_z,
    secondary_player_x,
    secondary_player_y,
    secondary_player_z,
    waypoint_x,
    waypoint_y,
)


class AC2Teleport(Teleport):
    ECHELON = 100

    def set_coords(self, x: Number, y: Number, z: Number | None = None):
        if z is None:
            with force_god_mode():
                self.player_z.value = self.ECHELON
                secondary_player_z.value = self.ECHELON
                sleep(1)
                self.player_x.value = x
                secondary_player_x.value = x
                self.player_y.value = y
                secondary_player_y.value = y
                sleep(15)
        else:
            super().set_coords(x, y, z)
            secondary_player_x.value = x
            secondary_player_y.value = y
            secondary_player_z.value = z

    def goto_waypoint(self):
        self.set_coords(waypoint_x.value, waypoint_y.value)


tp = AC2Teleport(
    primary_player_x,
    primary_player_y,
    primary_player_z,
)
