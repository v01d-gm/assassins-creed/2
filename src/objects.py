from contextlib import contextmanager

from trainerbase.gameobject import GameBool, GameByte, GameFloat, GameInt
from trainerbase.memory import Address

from memory import game_state_pointer, waypoint_coords_pointer


game_state_address = Address(game_state_pointer, [0x0])

invisible = GameBool(game_state_address + [0x1C])
god_mode = GameByte(game_state_address + [0x10, 0x20, 0x58, 0x8, 0x20], value_range=(128, 129))

inventory_address = game_state_address + [0x10, 0x58, 0xC, 0x10]
money = GameInt(inventory_address + [0x0, 0x10])
poison_darts = GameInt(inventory_address + [0x10, 0x10])
pistol_bullets = GameInt(inventory_address + [0x14, 0x10])
medicine = GameInt(inventory_address + [0xC, 0x10])
smoke_bombs = GameInt(inventory_address + [0x4, 0x10])
knives = GameByte(inventory_address + [0x20, 0x1E], value_range=(0, 15))

primary_player_coords_address = game_state_address + [0x8, 0x40]
primary_player_x = GameFloat(primary_player_coords_address)
primary_player_y = GameFloat(primary_player_coords_address + 0x4)
primary_player_z = GameFloat(primary_player_coords_address + 0x8)

secondary_player_x = GameFloat(0)
secondary_player_y = GameFloat(0)
secondary_player_z = GameFloat(0)

waypoint_coords_address = Address(waypoint_coords_pointer, [0x10])
waypoint_x = GameFloat(waypoint_coords_address)
waypoint_y = GameFloat(waypoint_coords_address + 0x4)


@contextmanager
def force_god_mode():
    original_value = god_mode.value
    god_mode.value = 0x81

    try:
        yield
    finally:
        god_mode.value = original_value
